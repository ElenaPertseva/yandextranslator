package gateway;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class YandexTranslateGateway extends Gateway {


    public String serialization(Object object){
        String json = gson.toJson(object);
        log.debug(json);
        return json;
    }

}
