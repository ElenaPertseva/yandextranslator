package entities;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class Translate {

    private String sourceLanguageCode;
    private String targetLanguageCode;
    private String format;
    private String[] texts;
    private String folderId;
    private String model;

}
