package entities;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class GlossaryPairs {

    private String sourceText;
    private String translatedText;

}
