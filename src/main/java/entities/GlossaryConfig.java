package entities;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class GlossaryConfig {

     private GlossaryData glossaryData;
}
