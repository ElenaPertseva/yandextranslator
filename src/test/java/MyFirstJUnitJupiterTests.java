import entities.Translate;
import gateway.Gateway;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class MyFirstJUnitJupiterTests {


      @Test
      @DisplayName("1 + 1 = 2")
        void addition() {
            System.out.println("Hello");
        }
      @Test
        void serialization(){
          String [] text = new String[]{"Hello", "World"};
          Translate translate = new Translate();
          translate.setFolderId("b1gs5lmjrf9itpcd06rs");
          translate.setTexts(text);
          translate.setTargetLanguageCode("ru");

          Gateway gateway = new Gateway();
           gateway.serialization(translate);
      }

}
